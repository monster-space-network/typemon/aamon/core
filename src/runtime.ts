import { Parameters, Exception } from './parameters';

export interface Runtime {
    initialize?(parameters: Parameters): void | Promise<void>;

    output?(output: unknown): void | Promise<void>;

    exception?(exception: Exception): void | Promise<void>;

    finalize?(): void | Promise<void>;
}
