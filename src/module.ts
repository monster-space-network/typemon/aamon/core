import { Constructor, Provider } from '@typemon/dependency-injection';

import { Handler } from './handler';
import { Middleware } from './middleware';

export interface Module {
    readonly handlers: ReadonlyArray<Handler.Constructor>;
    readonly middlewares?: ReadonlyArray<Middleware.Constructor>;
    readonly providers?: ReadonlyArray<Constructor | Provider>;
}
