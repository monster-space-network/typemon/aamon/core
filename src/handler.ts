import { Metadata } from '@typemon/metadata';
import { Injectable, Lifetime } from '@typemon/dependency-injection';

import { MetadataKey } from './metadata-key';
import { Middleware } from './middleware';

export interface Handler {
    execute(...parameters: any): unknown;
}
export function Handler(options: Handler.Options = {}): ClassDecorator {
    return Metadata.decorator((metadata: Metadata): void => {
        if (metadata.hasOwn(MetadataKey.OPTIONS)) {
            throw new Error('This decorator cannot be used in duplicate.');
        }

        metadata.set(MetadataKey.OPTIONS, options);
        metadata.decorate([
            Injectable(),
        ]);
    });
}
export namespace Handler {
    export type Constructor = new (...parameters: any) => Handler;

    export interface Options {
        readonly lifetime?: Lifetime;
        readonly middlewares?: ReadonlyArray<Middleware.Constructor>;
    }
}
