import { Inject } from '@typemon/dependency-injection';
import { Metadata } from '@typemon/metadata';

import { Handler } from '../handler';

export interface HandlerReference {
    readonly target: Handler.Constructor;
    readonly metadata: Metadata;
    readonly method: {
        readonly metadata: Metadata;
    };
}
export function HandlerReference(): ParameterDecorator {
    return Inject('aamon.core.parameters.handler-reference');
}
