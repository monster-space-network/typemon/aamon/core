import { Inject } from '@typemon/dependency-injection';

export interface Exception {
    readonly parent: null | Exception;
    readonly error: Error;
}
export function Exception(): ParameterDecorator {
    return Inject('aamon.core.parameters.exception');
}
