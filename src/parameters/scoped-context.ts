import { Check } from '@typemon/check';
import { Inject } from '@typemon/dependency-injection';

export type ScopedContext = Context;
export function ScopedContext(): ParameterDecorator {
    return Inject('aamon.core.parameters.scoped-context');
}

export class Context {
    private readonly data: Map<string | symbol, unknown>;

    public constructor() {
        this.data = new Map();
    }

    public has(key: string | symbol): boolean {
        return this.data.has(key);
    }
    public hasNot(key: string | symbol): boolean {
        return Check.isFalse(this.has(key));
    }

    public get(key: string | symbol): any {
        if (this.hasNot(key)) {
            throw new Error(`Key '${Check.isString(key) ? `String(${key})` : key.toString()}' does not exist.`);
        }

        return this.data.get(key);
    }

    public set(key: string | symbol, value: unknown): void {
        this.data.set(key, value);
    }

    public delete(key: string | symbol): void {
        this.data.delete(key);
    }
}
