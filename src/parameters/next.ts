import { Inject } from '@typemon/dependency-injection';

export class Nextable {
    public constructor(
        public readonly output?: unknown,
    ) { }
}

export type Next = (output?: unknown) => Nextable;
export function Next(): ParameterDecorator {
    return Inject('aamon.core.parameters.next');
}
