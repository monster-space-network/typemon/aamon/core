import { Check } from '@typemon/check';
import { Metadata } from '@typemon/metadata';
import { Identifier, AmbiguousProviderFoundError, ProviderNotFoundError, Inject } from '@typemon/dependency-injection';

import { ClassParameterMetadata, ObjectParameterMetadata, ParameterMetadata } from './parameter-metadata';

export namespace CustomParameterDecorator {
    export type Resolver = (parameters: ReadonlyParameters, metadata: ParameterMetadata) => unknown;

    export function create(resolver: Resolver): ParameterDecorator {
        return Metadata.decorator((metadata: Metadata): void => {
            metadata.decorate([
                Inject('aamon.core.parameters.custom'),
            ]);
            metadata.set('aamon.core.parameters.custom.resolver', resolver);
        });
    }
}

export type Resolver<Value = any> = (value: Value, metadata: ParameterMetadata) => unknown;

export interface ReadonlyParameters {
    has(identifier: Identifier): boolean;
    hasNot(identifier: Identifier): boolean;

    get(identifier: Identifier, metadata?: object | Metadata | ParameterMetadata): Promise<any>;
}

export class Parameters implements ReadonlyParameters {
    private readonly values: Map<Identifier, unknown>;
    private readonly resolvers: Map<Identifier, undefined | Resolver>;

    public constructor() {
        this.values = new Map();
        this.resolvers = new Map();
    }

    public has(identifier: Identifier): boolean {
        return this.values.has(identifier);
    }

    public hasNot(identifier: Identifier): boolean {
        return Check.isFalse(this.has(identifier));
    }

    public async get(identifier: Identifier, metadata: object | Metadata | ParameterMetadata = {}): Promise<any> {
        const parameterMetadata: ParameterMetadata = Check.instanceOf(metadata, ObjectParameterMetadata) || Check.instanceOf(metadata, ClassParameterMetadata)
            ? metadata
            : Metadata.is(metadata)
                ? new ClassParameterMetadata(metadata)
                : new ObjectParameterMetadata(metadata);

        if (Check.equal(identifier, 'aamon.core.parameters.custom')) {
            const resolver: CustomParameterDecorator.Resolver = parameterMetadata.get('aamon.core.parameters.custom.resolver');
            const value: unknown = await resolver(this, parameterMetadata);

            return value;
        }

        if (this.hasNot(identifier)) {
            if (parameterMetadata.optional) {
                return;
            }

            throw new ProviderNotFoundError(identifier);
        }

        const value: unknown = this.values.get(identifier);
        const resolver: undefined | Resolver = this.resolvers.get(identifier);

        if (Check.isUndefined(resolver)) {
            return value;
        }

        return resolver(value, parameterMetadata);
    }

    public set<Value>(identifier: Identifier, value: Value, resolver?: Resolver<Value>): void {
        if (this.has(identifier)) {
            throw new AmbiguousProviderFoundError(identifier);
        }

        this.resolvers.set(identifier, resolver);
        this.values.set(identifier, value);
    }
}
