import { Check } from '@typemon/check';
import { Metadata } from '@typemon/metadata';
import { InjectionOptions, MetadataKey } from '@typemon/dependency-injection';

export interface ParameterMetadata {
    readonly optional: boolean;

    has(key: string | symbol): boolean;
    hasNot(key: string | symbol): boolean;

    get(key: string | symbol): any;
}

export class ObjectParameterMetadata implements ParameterMetadata {
    public readonly optional: boolean;

    public constructor(
        private readonly metadata: object,
    ) {
        this.optional = this.has('optional')
            ? this.get('optional')
            : false;
    }

    public has(key: string | symbol): boolean {
        return Reflect.has(this.metadata, key);
    }
    public hasNot(key: string | symbol): boolean {
        return Check.isFalse(this.has(key));
    }

    public get(key: string | symbol): any {
        if (this.hasNot(key)) {
            throw new Error(`Metadata '${Check.isString(key) ? `String(${key})` : key.toString()}' does not exist.`);
        }

        return Reflect.get(this.metadata, key);
    }
}

export class ClassParameterMetadata implements ParameterMetadata {
    public readonly optional: boolean;

    public constructor(
        private readonly metadata: Metadata,
    ) {
        const options: InjectionOptions = this.has(MetadataKey.INJECTION_OPTIONS)
            ? this.get(MetadataKey.INJECTION_OPTIONS)
            : {};

        this.optional = options.optional ?? false;
    }

    public has(key: string | symbol): boolean {
        return this.metadata.hasOwn(key);
    }
    public hasNot(key: string | symbol): boolean {
        return this.metadata.hasNotOwn(key);
    }

    public get(key: string | symbol): any {
        return this.metadata.getOwn(key);
    }
}
