import { Metadata } from '@typemon/metadata';
import { Injectable, Lifetime } from '@typemon/dependency-injection';

import { MetadataKey } from './metadata-key';

export interface Middleware {
    pre?(...parameters: any): unknown;
    post?(...parameters: any): unknown;
}
export function Middleware(options: Middleware.Options = {}): ClassDecorator {
    return Metadata.decorator((metadata: Metadata): void => {
        if (metadata.hasOwn(MetadataKey.OPTIONS)) {
            throw new Error('This decorator cannot be used in duplicate.');
        }

        metadata.set(MetadataKey.OPTIONS, options);
        metadata.decorate([
            Injectable(),
        ]);
    });
}
export namespace Middleware {
    export type Constructor = new (...parameters: any) => Middleware;

    export interface Options {
        readonly lifetime?: Lifetime;
    }
}
