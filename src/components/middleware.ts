import { Check } from '@typemon/check';

import { MetadataKey } from '../metadata-key';
import { Middleware } from '../middleware';
import { ClassComponent } from './class';
import { MethodComponent } from './method';
import { ParameterComponent } from './parameter';

export class MiddlewarePreComponent extends MethodComponent {
    public readonly flowControllable: boolean;

    public constructor(target: Object) {
        super(target, 'pre');

        this.flowControllable = this.parameters.some((parameter: ParameterComponent): boolean => Check.equal(parameter.identifier, 'aamon.core.parameters.next'));
    }
}

export class MiddlewarePostComponent extends MethodComponent {
    public readonly exceptionHandleable: boolean;

    public constructor(target: Object) {
        super(target, 'post');

        this.exceptionHandleable = this.parameters.some((parameter: ParameterComponent): boolean => Check.equal(parameter.identifier, 'aamon.core.parameters.exception'));
    }
}

export class MiddlewareComponent extends ClassComponent {
    public readonly options: Middleware.Options;

    public readonly pre: null | MiddlewarePreComponent;
    public readonly post: null | MiddlewarePostComponent;

    public constructor(
        public readonly target: Middleware.Constructor,
    ) {
        super(target);

        this.options = this.metadata.getOwn(MetadataKey.OPTIONS);

        this.pre = Check.isIn('pre', this.target.prototype)
            ? new MiddlewarePreComponent(this.target.prototype)
            : null;
        this.post = Check.isIn('post', this.target.prototype)
            ? new MiddlewarePostComponent(this.target.prototype)
            : null;
    }
}
