import { Metadata } from '@typemon/metadata';
import { MetadataKey, Identifier } from '@typemon/dependency-injection';

export class ParameterComponent {
    public readonly metadata: Metadata;

    public readonly identifier: Identifier;

    public constructor(target: Object, propertyKey: string, parameterIndex: number) {
        this.metadata = Metadata.of({
            target,
            propertyKey,
            parameterIndex,
        });

        this.identifier = this.metadata.getOwn(MetadataKey.IDENTIFIER);
    }
}
