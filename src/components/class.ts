import { Metadata } from '@typemon/metadata';

export class ClassComponent {
    public readonly metadata: Metadata;

    public constructor(target: Function) {
        this.metadata = Metadata.of({
            target,
        });
    }
}
