import { MetadataKey } from '../metadata-key';
import { Handler } from '../handler';
import { Middleware } from '../middleware';
import { HandlerReference } from '../parameters';
import { ClassComponent } from './class';
import { MethodComponent } from './method';
import { MiddlewareComponent } from './middleware';

export class HandlerComponent extends ClassComponent implements HandlerReference {
    public readonly options: Handler.Options;

    public readonly method: MethodComponent;

    public readonly middlewares: ReadonlyArray<MiddlewareComponent>;

    public constructor(
        public readonly target: Handler.Constructor,
    ) {
        super(target);

        this.options = this.metadata.getOwn(MetadataKey.OPTIONS);

        this.method = new MethodComponent(this.target.prototype, 'execute');

        this.middlewares = (this.options.middlewares ?? []).map((target: Middleware.Constructor): MiddlewareComponent => new MiddlewareComponent(target));
    }
}
