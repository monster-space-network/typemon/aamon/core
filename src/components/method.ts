import { Metadata } from '@typemon/metadata';
import { MetadataKey } from '@typemon/dependency-injection';

import { ParameterComponent } from './parameter';

export class MethodComponent {
    public readonly metadata: Metadata;

    public readonly parameters: ReadonlyArray<ParameterComponent>;

    public constructor(target: Object, propertyKey: string) {
        this.metadata = Metadata.of({
            target,
            propertyKey,
        });

        const parameterTypes: ReadonlyArray<unknown> = this.metadata.hasOwn(MetadataKey.DESIGN_PARAMETER_TYPES)
            ? this.metadata.getOwn(MetadataKey.DESIGN_PARAMETER_TYPES)
            : [];
        const parameters: ReadonlyArray<ParameterComponent> = parameterTypes.map((_: unknown, parameterIndex: number): ParameterComponent => new ParameterComponent(target, propertyKey, parameterIndex));

        this.parameters = parameters;
    }
}
