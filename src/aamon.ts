import { Check } from '@typemon/check';
import { Scope } from '@typemon/scope';
import { Container, Identifier, Lifetime } from '@typemon/dependency-injection';

import { Module } from './module';
import { Executor, ExecutionContext } from './execution';
import { Handler } from './handler';
import { Middleware } from './middleware';
import { HandlerComponent, MiddlewareComponent } from './components';
import { Exception } from './parameters';
import { Runtime } from './runtime';

export class Aamon {
    public static async create(module: Module): Promise<Aamon> {
        const aamon: Aamon = new Aamon(module);

        await aamon.initialize();

        return aamon;
    }

    private readonly container: Container;

    private readonly handlers: ReadonlyArray<Handler.Constructor>;
    private readonly handlerComponents: Map<Handler.Constructor, HandlerComponent>;

    private readonly middlewares: ReadonlyArray<Middleware.Constructor>;
    private readonly middlewareComponents: Map<Middleware.Constructor, MiddlewareComponent>;

    private readonly initializationTargets: Array<Identifier>;

    private constructor(module: Module) {
        this.container = new Container({
            scope: 'aamon.core.execution',
        });

        this.handlers = module.handlers;
        this.handlerComponents = new Map();

        this.middlewares = module.middlewares ?? [];
        this.middlewareComponents = new Map();

        this.initializationTargets = [];

        for (const target of this.handlers) {
            if (this.handlerComponents.has(target)) {
                throw new Error('Handlers must not be duplicated.');
            }

            const component: HandlerComponent = new HandlerComponent(target);

            for (const middleware of component.middlewares) {
                if (this.middlewareComponents.has(middleware.target)) {
                    continue;
                }

                this.middleware(middleware);
            }

            this.handlerComponents.set(target, component);
            this.container.bind({
                identifier: target,
                useClass: target,
                lifetime: component.options.lifetime,
            });

            if (Check.equal(component.options.lifetime ?? Lifetime.Singleton, Lifetime.Singleton)) {
                this.initializationTargets.push(target);
            }
        }

        for (const middleware of this.middlewares) {
            if (this.middlewareComponents.has(middleware)) {
                continue;
            }

            this.middleware(new MiddlewareComponent(middleware));
        }

        for (const provider of module.providers ?? []) {
            this.container.bind(provider);

            if (Check.isIn('useClass', provider) || Check.isIn('useFactory', provider)) {
                if (Check.equal(provider.lifetime ?? Lifetime.Singleton, Lifetime.Singleton)) {
                    this.initializationTargets.push(provider.identifier);
                }
            }
        }
    }

    private middleware(component: MiddlewareComponent): void {
        if (Check.isNull(component.pre) && Check.isNull(component.post)) {
            throw new Error('Middleware must implement at least one method.');
        }

        this.middlewareComponents.set(component.target, component);
        this.container.bind({
            identifier: component.target,
            useClass: component.target,
            lifetime: component.options.lifetime,
        });

        if (Check.equal(component.options.lifetime ?? Lifetime.Singleton, Lifetime.Singleton)) {
            this.initializationTargets.push(component.target);
        }
    }

    private async initialize(): Promise<void> {
        for (const target of this.initializationTargets) {
            await this.container.get(target);
        }
    }

    public async execute(runtime: Runtime, handler: Handler.Constructor): Promise<null | Exception> {
        const handlerComponent: undefined | HandlerComponent = this.handlerComponents.get(handler);

        if (Check.isUndefined(handlerComponent)) {
            throw new Error('Handler not initialized.');
        }

        const context: ExecutionContext = new ExecutionContext(
            this.container,
            runtime,
            handlerComponent,
            this.middlewares
                .map((target: Middleware.Constructor): MiddlewareComponent => this.middlewareComponents.get(target) as MiddlewareComponent)
                .concat(handlerComponent.middlewares),
        );

        return Scope.run('aamon.core.execution', (): Promise<null | Exception> => Executor.execute(context));
    }
}
