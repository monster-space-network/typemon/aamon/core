import { Container } from '@typemon/dependency-injection';

import { Runtime } from '../runtime';
import { HandlerComponent, MiddlewareComponent } from '../components';
import { Parameters, Nextable, Exception, Context } from '../parameters';

export class ExecutionContext {
    public middlewareLevel: number;
    public exception: null | Exception;
    public readonly parameters: Parameters;

    public constructor(
        public readonly container: Container,
        public readonly runtime: Runtime,
        public readonly handler: HandlerComponent,
        public readonly middlewares: ReadonlyArray<MiddlewareComponent>,
    ) {
        this.middlewareLevel = 0;
        this.exception = null;
        this.parameters = new Parameters();
        this.parameters.set('aamon.core.parameters.next', (output?: unknown): Nextable => new Nextable(output));
        this.parameters.set('aamon.core.parameters.exception', null, (): null | Exception => this.exception);
        this.parameters.set('aamon.core.parameters.handler-reference', this.handler);
        this.parameters.set('aamon.core.parameters.scoped-context', new Context());
    }
}
