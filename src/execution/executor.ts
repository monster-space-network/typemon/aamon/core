import { Check } from '@typemon/check';
import { Metadata } from '@typemon/metadata';
import { Container } from '@typemon/dependency-injection';
import { Pipeline } from '@typemon/pipeline';

import { MethodComponent } from '../components';
import { Exception, Nextable } from '../parameters';
import { Handler } from '../handler';
import { Middleware } from '../middleware';
import { ExecutionContext } from './execution-context';
import { ROP } from './rop';

export namespace Executor {
    async function apply(value: unknown, metadata: Metadata, container: Container): Promise<unknown> {
        return Pipeline.resolve({
            value,
            pipes: Pipeline.of(metadata),
            container,
        });
    }

    async function* resolve(method: MethodComponent, context: ExecutionContext): AsyncGenerator<ROP, ReadonlyArray<unknown>> {
        const previousException: null | Exception = context.exception;
        const parameters: Array<unknown> = [];

        for (const component of method.parameters) {
            const parameter: unknown = yield ROP.unhandleable((): unknown => context.parameters.get(component.identifier, component.metadata));
            const pipedParameter: unknown = yield ROP.handleable((): unknown => apply(parameter, component.metadata, context.container));

            if (Check.notEqual(context.exception, previousException)) {
                break;
            }

            parameters.push(pipedParameter);
        }

        return parameters;
    }

    async function* pre(context: ExecutionContext): AsyncGenerator<ROP, boolean, any> {
        for (const middleware of context.middlewares) {
            if (Check.isNull(middleware.pre)) {
                context.middlewareLevel += 1;

                continue;
            }

            const instance: Middleware = yield ROP.unhandleable((): unknown => context.container.get(middleware.target));
            const parameters: ReadonlyArray<unknown> = yield* resolve(middleware.pre!, context);

            if (Check.isNotNull(context.exception)) {
                return false;
            }

            const result: unknown = yield ROP.handleable((): unknown => instance.pre!(...parameters));

            if (Check.isNotNull(context.exception)) {
                return false;
            }

            const [stop, output]: [boolean, unknown] = middleware.pre.flowControllable
                ? Check.instanceOf(result, Nextable)
                    ? [false, result.output]
                    : [true, result]
                : [false, result];
            const pipedOutput: unknown = yield ROP.handleable((): unknown => apply(output, middleware.pre!.metadata, context.container));

            if (Check.isNotNull(context.exception)) {
                return false;
            }

            yield ROP.unhandleable((): unknown => context.runtime.output?.(pipedOutput));

            context.middlewareLevel += 1;

            if (stop) {
                return false;
            }
        }

        return true;
    }

    async function* post(context: ExecutionContext): AsyncGenerator<ROP, void, any> {
        for (const middleware of context.middlewares.slice(0, context.middlewareLevel).reverse()) {
            if (Check.isNull(middleware.post)) {
                continue;
            }

            if (Check.isNotNull(context.exception) && Check.isFalse(middleware.post.exceptionHandleable)) {
                continue;
            }

            const previousException: null | Exception = context.exception;
            const instance: Middleware = yield ROP.unhandleable((): unknown => context.container.get(middleware.target));
            const parameters: ReadonlyArray<unknown> = yield* resolve(middleware.post!, context);

            if (Check.notEqual(context.exception, previousException)) {
                continue;
            }

            const output: unknown = yield ROP.handleable((): unknown => instance.post!(...parameters));

            if (Check.notEqual(context.exception, previousException)) {
                continue;
            }

            const pipedOutput: unknown = yield ROP.handleable((): unknown => apply(output, middleware.post!.metadata, context.container));

            if (Check.notEqual(context.exception, previousException)) {
                continue;
            }

            context.exception = null;

            yield ROP.unhandleable((): unknown => context.runtime.output?.(pipedOutput));
        }
    }

    async function* main(context: ExecutionContext): AsyncGenerator<ROP, void, any> {
        const instance: Handler = yield ROP.unhandleable((): unknown => context.container.get(context.handler.target));
        const parameters: ReadonlyArray<unknown> = yield* resolve(context.handler.method, context);

        if (Check.isNotNull(context.exception)) {
            return;
        }

        const output: unknown = yield ROP.handleable((): unknown => instance.execute(...parameters));

        if (Check.isNotNull(context.exception)) {
            return;
        }

        const pipedOutput: unknown = yield ROP.handleable((): unknown => apply(output, context.handler.method.metadata, context.container));

        if (Check.isNotNull(context.exception)) {
            return;
        }

        yield ROP.unhandleable((): unknown => context.runtime.output?.(pipedOutput));
    }

    async function* ready(context: ExecutionContext): AsyncGenerator<ROP, void> {
        yield ROP.unhandleable((): unknown => context.runtime.initialize?.(context.parameters));

        const nextable: boolean = yield* pre(context);

        if (nextable) {
            yield* main(context);
        }

        yield* post(context);

        if (Check.isNotNull(context.exception) && Check.isNotUndefined(context.runtime.exception)) {
            yield ROP.unhandleable((): unknown => context.runtime.exception!(context.exception!));

            context.exception = null;
        }

        yield ROP.unhandleable((): unknown => context.runtime.finalize?.());
    }

    export async function execute(context: ExecutionContext): Promise<null | Exception> {
        const runner: AsyncGenerator<ROP, void> = ready(context);

        for (let result: IteratorResult<ROP> = await runner.next(null), value: unknown = null; Check.isFalse(result.done); value = result.value.value, result = await runner.next(value)) {
            if (Check.isNull(result.value.error)) {
                continue;
            }

            context.exception = {
                parent: context.exception,
                error: result.value.error,
            };

            if (Check.isFalse(result.value.handleable)) {
                break;
            }
        }

        return context.exception;
    }
}
