export interface ROP {
    readonly value: unknown;
    readonly error: null | Error;
    readonly handleable: boolean;
}
export namespace ROP {
    async function execute(handleable: boolean, callback: () => unknown): Promise<ROP> {
        try {
            return {
                value: await callback(),
                error: null,
                handleable,
            };
        }
        catch (error) {
            return {
                value: null,
                error,
                handleable,
            };
        }
    }

    export async function handleable(callback: () => unknown): Promise<ROP> {
        return execute(true, callback);
    }

    export async function unhandleable(callback: () => unknown): Promise<ROP> {
        return execute(false, callback);
    }
}
