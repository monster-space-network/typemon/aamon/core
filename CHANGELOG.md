# [1.0.0-next.6](https://gitlab.com/monster-space-network/typemon/aamon/core/compare/1.0.0-next.5...1.0.0-next.6) (2021-03-12)


### Bug Fixes

* **execution.executor.pre:** 누락된 레벨 증가 처리 추가 ([deeb930](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/deeb93017b1505b34ed2a5d9b461505d3487fff5))



# [1.0.0-next.5](https://gitlab.com/monster-space-network/typemon/aamon/core/compare/1.0.0-next.4...1.0.0-next.5) (2021-02-12)


### Bug Fixes

* **execution.executor:** 잘못된 예외 처리 수정 ([aa915f5](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/aa915f5ed3263e3eec91be8a63140adf5c322d55))



# [1.0.0-next.4](https://gitlab.com/monster-space-network/typemon/aamon/core/compare/1.0.0-next.3...1.0.0-next.4) (2021-01-31)


### Bug Fixes

* **aamon:** 잘못된 수명 조건 수정 ([bf05508](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/bf0550844e3fb62e077c41dc1713f15a75f9e89d))
* **aamon:** 잘못된 컨트롤러 미들웨어 초기화 대상 처리 수정 ([1232ecd](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/1232ecd097c43b4d2dd9a7675633174106bfd941))
* **execution.executor:** 누락된 처리 가능한 예외 처리 추가 ([8b0169c](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/8b0169cd57b193d9e8ffa781e7c4e57e6bd7ec70))


### Features

* **aamon:** 미들웨어 중복 방지 기능 삭제 ([32d8714](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/32d87147fb485b8311fb44a80b5d672c1ce933c0))
* **execution.executor:** 메서드 파이프라인 지원 추가 ([066e207](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/066e2074d38ee28ed85e3f816de3dfcc1438be9a))
* 커스텀 파라미터 데코레이터 기능 추가 ([ac25fad](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/ac25fad3df94b5df8d04c8ed35b37671ba38d71e))
* **execution.executor:** 파라미터 예외 처리 방식 변경 ([dfb66c6](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/dfb66c6d2bb117e7223c79ccf0f8bf6d002c23b2))
* **parameters:** 식별자 구조 변경 ([260cc22](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/260cc227a31f952849460d485a5dc513ac737e8a))
* **parameters.execution:** handleable 속성 삭제 ([a12ee55](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/a12ee555ff558ab74bedff02442fc581a07a8f28))



# [1.0.0-next.3](https://gitlab.com/monster-space-network/typemon/aamon/core/compare/1.0.0-next.2...1.0.0-next.3) (2021-01-28)


### Bug Fixes

* **execution.executor:** 종료 시 오류 발생 문제 해결 ([19f6678](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/19f66783e0296159889043c66f112764b3b29d94))



# [1.0.0-next.2](https://gitlab.com/monster-space-network/typemon/aamon/core/compare/1.0.0-next.1...1.0.0-next.2) (2021-01-28)


### Features

* **execution:** 파라미터 파이프라인 지원 추가 ([57d9264](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/57d9264c136086c5e7e57a65b8c52a0a3c003076))
* @typemon/metadata 의존성 업데이트 ([c06bbf2](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/c06bbf2d89a10ce3924c6c8ce90f81af9f00f5f9))
* @typemon/pipeline 의존성 추가 ([168fe28](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/168fe2812569522c0067e5c71430e83cdff817ce))



# [1.0.0-next.1](https://gitlab.com/monster-space-network/typemon/aamon/core/compare/1.0.0-next.0...1.0.0-next.1) (2021-01-24)


### Bug Fixes

* **execution.executor:** 누락된 예외 널 처리 추가 ([4841c3a](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/4841c3a0f3899d90b550fba99ca48ac873844520))
* **parameters:** 잘못된 메타데이터 처리 방식 수정 ([3743a64](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/3743a64b06e163327b1752ed87a7252c77863a34))


### Features

* **runtime:** 옵셔널 적용 ([03c8241](https://gitlab.com/monster-space-network/typemon/aamon/core/commit/03c8241c23a1622caa283079422e7668944f219a))



